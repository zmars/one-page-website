$(document).ready(function(){

/** INITIALIZE DISPLAYS **/
$(".menu-toggle").removeClass("close");
$(".contentBackground").hide();
fullpage.moveTo(1, 0);

/***********************************************************/

/** NAVIGATION BUTTON CONTROLLER **/
/** Browser Navigation Buttons **/
$(".naviBtnDiv button").click(function(){
	$(".naviBtnDiv button").removeClass("active");
	$(this).addClass("active");
	var navigateTo = $(this).attr("data-btnmenu-value");
	fullpage.moveTo(navigateTo, 0);
});

/** Burger Navigation Buttons **/
$(".menu-toggle").click(function(){
	console.log("Test burger.");
	var status = $(this).attr("class");

	if(status=="menu-toggle close")
	{
		$(this).removeClass("close");
		$(".contentBackground").fadeOut();
	}else
	{
		$(this).addClass("close");
		$(".contentBackground").fadeIn();
	}
});


/***********************************************************/

/** EVENT SLIDER CONTROLLER **/
setInterval(function(){ 
	$(".fp-next").click();
}, 5000);

/** ABOUT US CONTROLLER **/
setInterval(function(){
	var currentImage = $('.aboutContent .about-item.active');
    var currentImageIndex = $('.aboutContent .about-item.active').index();
    var nextImageIndex = currentImageIndex + 1;
    var nextImage = $('.aboutContent .about-item').eq(nextImageIndex);

    var currentPage = $('.aboutPagination .page.active');
    var currentPageIndex = $('.aboutPagination .page.active').index();
    var nextPageIndex = currentPageIndex + 1;
    var nextPage = $('.aboutPagination .page').eq(nextPageIndex);

    currentImage.fadeOut(500, function(){
		currentImage.removeClass("active");
		currentPage.removeClass("active");

		if (nextImageIndex == ($('.aboutContent .about-item:last').index() + 1)) {
			$('.aboutPagination .page').eq(0).addClass('active');
      		$('.aboutContent .about-item').eq(0).fadeIn(500);
     		$('.aboutContent .about-item').eq(0).addClass('active');
   		} else {
   			nextPage.addClass('active');
      		nextImage.fadeIn(500);
      		nextImage.addClass('active');
    	}
	});
}, 3000);


/***********************************************************/

$("body").scroll(function(){
	console.log("SCROLLING!");
	var url = window.location.href; 
    var page = url.split('#');
    var curry = page[1];
    if(page.indexOf('/') > 1)
    {
        page = curry.split['/'];
        curry = page[0];
    }
        
    $(".naviBtnDiv button").removeClass("active");
    $(".naviBtnDiv button:nth-child("+curry+")").addClass("active");
});

});